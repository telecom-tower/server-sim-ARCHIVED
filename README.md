# towerserver simulator

## Build

  1. Install "esc":
  
    > go get -i github.com/mjibson/esc
    
  2. Make sure that $GOPATH/bin is in your path and
     that you can call "esc". You can check with the
     "which" command:
     
    > which esc
    
    /home/you/go/bin/esc
    
  3. Build the static files:
  
    > go generate
   
  4. Build the binary:
  
    > go build